package be.kdg.personsrest.model

import com.squareup.moshi.Json

data class Person(
    val id: Int,
    @Json(name = "firstName")
    val givenName: String,
    val lastName: String,
    val yearsService: Int,
    val gender: String,
    val image: String,
    val married: Boolean
)
