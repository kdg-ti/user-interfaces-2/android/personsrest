package be.kdg.personsrest.controllers

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.*
import be.kdg.personsrest.R
import be.kdg.personsrest.controllers.adapters.PersonsAdapter
import be.kdg.personsrest.controllers.viewmodels.OverviewModel
import be.kdg.personsrest.databinding.OverviewFragmentBinding

class OverviewFragment : Fragment() {

    private lateinit var binding: OverviewFragmentBinding

    private val viewModel: OverviewModel by viewModels()
//    private val viewModel: OverviewModel by lazy {
//        ViewModelProvider(this).get(OverviewModel::class.java)
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.overview_fragment, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val personsAdapter = PersonsAdapter()
        binding.persons.apply {
            adapter = personsAdapter
        }

        viewModel.persons.observe(viewLifecycleOwner) {
            Log.d("OverviewFrament", "persons loaded ${it.size}")
            personsAdapter.submitList(it)
        }
    }
}
