package be.kdg.personsrest.controllers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.personsrest.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
