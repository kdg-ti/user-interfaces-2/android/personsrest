package be.kdg.personsrest.controllers.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import be.kdg.personsrest.R
import be.kdg.personsrest.databinding.PersonViewBinding
import be.kdg.personsrest.model.Person

// We bouwen een zeer eenvoudige adapter, aangezien dat niet de focus van de oefening is.
// De UI voor één rij is een TextView met een  toString van 1 Person
class PersonsAdapter : ListAdapter<Person, PersonsAdapter.PersonViewHolder>(DiffCallback) {

    class PersonViewHolder(private var binding: PersonViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(person: Person) {
            binding.person = person
            // binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        return PersonViewHolder(PersonViewBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(viewHolder: PersonViewHolder, i: Int) {
        viewHolder.bind(getItem(i))
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Person>() {
        override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem.id == newItem.id
        }
    }
}
