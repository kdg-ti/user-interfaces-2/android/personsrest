package be.kdg.personsrest.controllers.viewmodels

import android.util.Log
import androidx.lifecycle.*
import be.kdg.personsrest.model.Person
import be.kdg.personsrest.services.rest.PersonAPI
import kotlinx.coroutines.launch

class OverviewModel : ViewModel() {

    private val _persons: MutableLiveData<List<Person>> = MutableLiveData()

    val persons: LiveData<List<Person>>
        get() = _persons

    init {
        getPersons()
    }

    private fun getPersons() {
        viewModelScope.launch {
            try {
                val personsList = PersonAPI.retrofitService.getPersons()
                Log.d("OverviewModel", "persons loaded by Retrofit ${personsList.size}")
                _persons.value = personsList
            } catch (e: Exception) {
                Log.d("OverviewModel", "Error loading ${e.message}")
                _persons.value = ArrayList()
            }
        }
    }
}
