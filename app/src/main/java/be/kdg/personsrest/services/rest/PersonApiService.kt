package be.kdg.personsrest.services.rest

import be.kdg.personsrest.model.Person
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

private const val BASE_URL: String = "http://10.0.2.2:3000/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit =
    Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

interface PersonApiService {
    @GET("persons")
    suspend fun getPersons(): List<Person>
}

object PersonAPI {
    val retrofitService: PersonApiService by lazy {
        retrofit.create(PersonApiService::class.java)
    }
}
